
# **Skykid.Me** - Timers and links for Sky: Children of the Light

## Questions & Anwsers

### Can my channel be included?
If it meets requirements. Is informative, has active content.

### Why is this project not hosted on GitHub, Gitlab or SourceForge?
Because none of those options are suitable for open source projects and are activly discouraged by [The Software Freedom Conservancy](https://sfconservancy.org/). Acting against copyleft licenses, abusing licensing or all out ignoring it are just tip of the iceberg. If you want to learn more read the following article: [Give Up GitHub!](https://sfconservancy.org/GiveUpGitHub/)

## Technology used
![Codeberg Badge](https://img.shields.io/badge/Codeberg-2185D0?logo=codeberg&logoColor=fff&style=for-the-badge) ![Pug Badge](https://img.shields.io/badge/Pug-A86454?logo=pug&logoColor=fff&style=for-the-badge) ![Stylus Badge](https://img.shields.io/badge/Stylus-333?logo=stylus&logoColor=fff&style=for-the-badge)   ![Node.js Badge](https://img.shields.io/badge/Node.js-393?logo=nodedotjs&logoColor=fff&style=for-the-badge) ![JSON Badge](https://img.shields.io/badge/JSON-000?logo=json&logoColor=fff&style=for-the-badge) 

All operations are executed on client-side only.

## Support

![Liberapay](https://img.shields.io/liberapay/receives/tiritto.svg?logo=liberapay&style=for-the-badge) ![Buy Me A Coffee Badge](https://img.shields.io/badge/Buy%20Me%20A%20Coffee-FD0?logo=buymeacoffee&logoColor=000&style=for-the-badge)

