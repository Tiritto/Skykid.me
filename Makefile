.DEFAULT_GOAL := help
CONTAINER_NAME?=skykid
CONTAINER_DAEMON?=podman

# Creates a timestamps for Make targets
TIMESTAMPED:=.make/timestamps
define CREATE_TIMESTAMP
	mkdir --parents ${TIMESTAMPED}
	touch ${TIMESTAMPED}/$(strip $(1))
endef

# Runs a container using default settings
define RUN_CONTAINER
	${CONTAINER_DAEMON} container run \
		--userns=keep-id \
		--name ${CONTAINER_NAME} \
		--rm \
		--tty \
		--interactive \
		--volume "${CURDIR}/package.json":/app/package.json \
		--volume "${CURDIR}":/app/project
endef

help: ## Displays this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
# @egrep -h '\s##\s' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-30s\033[0m %s\n", $$1, $$2}'

$(TIMESTAMPED)/image: ./container/Containerfile ./container/package.json
	@echo "Image must be rebuild. Starting..."
	$(call CREATE_TIMESTAMP, image)
	${CONTAINER_DAEMON} build \
		--file ./container/Containerfile \
		--tag ${CONTAINER_NAME} \
		--no-cache \
		.

image: ## Builds an Image if it doesn't exist or needs to be updated
	@echo "Checking if container named ${CONTAINER_NAME} exists..."
	@CONTAINER_ID=$(${CONTAINER_DAEMON} ps -aqf "name=${CONTAINER_NAME}"); \
	if [ -n "$${CONTAINER_ID}" ]; then \
		echo "Container '${CONTAINER_NAME}' already exists."; \
		make $(TIMESTAMPED)/image; \
	else \
		echo "Container ${CONTAINER_NAME} does not exist. Forcing creation..."; \
		make --always-make $(TIMESTAMPED)/image; \
	fi

npm: image ## Starts the container and executes requested commands via npm
	@echo "Running container with following arguments for npm: $(arguments)"
	@${RUN_CONTAINER} ${CONTAINER_NAME} run $(arguments)

types: image ## Generates types used by the compiler and exposes them to the project
	rm --recursive --force ./types && mkdir ./types
	${RUN_CONTAINER} --volume "${CURDIR}/types":/app/types ${CONTAINER_NAME} run exportTypes

build:
	make npm arguments=build

live: image ## Runs the container in parcel's live preview mode on port 1234
	${RUN_CONTAINER} --publish 1234:1234 ${CONTAINER_NAME} run serve

remove-container: ## Removes pre-existing containers with the same name
	@echo "Checking for container named ${CONTAINER_NAME}..."
	@CONTAINER_ID=$(${CONTAINER_DAEMON} ps -aqf "name=${CONTAINER_NAME}"); \
	if [ -n "$${CONTAINER_ID}" ]; then \
		echo "Container '${CONTAINER_NAME}' already exists. Removing it..."; \
		${CONTAINER_DAEMON} container stop $${CONTAINER_ID}; \
		${CONTAINER_DAEMON} container rm $${CONTAINER_ID}; \
	else \
		echo "Container ${CONTAINER_NAME} does not exist yet."; \
	fi

stop: ## Stops running builder
	${CONTAINER_DAEMON} container stop ${CONTAINER_NAME}

clean: remove-container ## Removes all temporary data used by the builder. Stops the builder in the process.
	${CONTAINER_DAEMON} rm skykid ${CONTAINER_NAME}