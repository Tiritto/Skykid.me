import { DateTime } from "luxon";
import { StandardEvent } from "./typescript/eventFunctions";
import { SkyTime, getNextConcertTime } from "./typescript/timeFunctions";
import { Countdown } from "./typescript/countdownFunctions";

const currentSeasonEnd: DateTime = DateTime.local(2023, 4, 2, 23, 59, 59, 0, { zone: 'America/Los_Angeles' });
const skyTime = new SkyTime();

// Update local/server clocks
skyTime.addEvent((skyTime) => {
    document.getElementById("local-time-value").innerHTML = new Date().toLocaleTimeString();
    document.getElementById("server-time-value").innerHTML = skyTime.toLocaleString({ hour: '2-digit', minute: '2-digit' , second: '2-digit' });
});

// The Polluted Geyser erupts Darkness spiky balls that give pieces of light when burned. 
// The first eruption starts at reset at 00:00 PST/PDT ,repeats every two hours and last 15 minutes.
// The start is signified by the seashell floating up and unclogging the geyser.
// During the first 5 minutes, the five vents around the geyser blow out steam. Each cycle of the vents represents 1 minute passing.
// Then, for 10 minutes, the geyser fills up and Darkness spiky balls erupt from the geyser every minute (for a total of 11 eruptions).
// Once it erupts, the geyser drains until it fills up for the next eruption.
const geyser = new StandardEvent(skyTime, "geyser", 5, 10);

// Like the Geyser, this event repeats every two hours and lasts for 15 minutes.
// The spirit appears at 00:30 PST/PDT, waits 5 minutes, and then proceeds to "serve dinner" for 10 minutes.
// Every minute, 6 white balls (that give 10 pieces of light each) appear on the table that give pieces of light when burned. 
// At the end of the event you can hug Grandma before she leaves.
const grandma = new StandardEvent(skyTime, "grandma", 35, 10);

// The Sunset event occurs in the Sanctuary Islands in Daylight Prairie.
// It starts at 00:50 PST/PDT, repeats every two hours and lasts for 10 minutes.
// There are several sources of light that appear only during this period, in and around the deep underwater ravine.
const turtle = new StandardEvent(skyTime, "turtle", 50, 10);

// Concert will repeat every 4 hours starting at midnight 00:00 PST (UTC-8)
const aurora  = new Countdown(skyTime, "aurora", (dt) => getNextConcertTime(dt));

// Dailies restart each day
const dailies = new Countdown(skyTime, "dailies", (dt) => dt.endOf('day'));
const eden    = new Countdown(skyTime, "eden", (dt) => dt.endOf('week').minus({ days: 1 }));
const season  = new Countdown(skyTime, "season", () => currentSeasonEnd);