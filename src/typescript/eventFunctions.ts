import { SkyTime, getNextEventTime, hmsToSeconds, humanizeSeconds, secondsToClock, twoHoursInSeconds } from "./timeFunctions";
import { getCountdownFromElement } from "./getCountdownFromElement";

enum EventStatus {
    Inactive,
    Active
}

export class StandardEvent {
    private _element: HTMLElement;
    private _countdown: HTMLOutputElement;
    private _startingMinute: number;
    private _duration: number;
    private _timer: SkyTime;

    constructor(skyTime: SkyTime, elementId: string, startingMinute: number, duration: number) {
        this._element = document.getElementById(elementId);

        this._countdown = this._element.getElementsByClassName('countdown')[0] as HTMLOutputElement;
        this._startingMinute = startingMinute;
        this._duration = duration;
        this._timer = skyTime;
        this._timer.addEvent(this.update);
    }

    /** Returns amount of seconds since the last time event started */
    public getSecondsSinceStarted = () => {
        return getNextEventTime(this._timer.now, this._startingMinute).diffNow("seconds").seconds;
    }

    /** Returns amount seconds for which event will stay active, or -1 if event is no longer active */
    public getTimeRemaining() {
        const secondsSinceStarted = this.getSecondsSinceStarted();
        return secondsSinceStarted > this._duration ? -1 : secondsSinceStarted - this._duration;
    }

    private update = () => getCountdownFromElement(this._element).innerHTML = secondsToClock(this.getSecondsSinceStarted());
}
