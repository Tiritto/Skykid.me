import { DateTime } from "luxon";
export const twoHoursInSeconds = 7200; // 7200 seconds = 2 hours

export class SkyTime {
    public now: DateTime;
    public hour: number;
    public minute: number;
    public second: number;

    private _intervalNumber: number;
    private _events: Array<(a: DateTime) => void> = [];

    constructor() {
        this._intervalNumber = setInterval(() => this.update(), 1000);
    }

    private update() {
        this.now = DateTime.now().setZone('America/Los_Angeles');
        this.hour = this.now.hour;
        this.minute = this.now.minute;
        this.second = this.now.second;
        this.executeEvents(this.now);
    }

    private executeEvents = (dt: DateTime) => this._events.forEach(event => event(dt));
    public addEvent = (fn: (a: DateTime) => void) => this._events.push(fn);

    /** Checks if supplied hour is an event hour */
    static isEventHour = (hour: number) => hour === 0 || hour % 2 === 0;

    /** Returns last Event hour */
    public getLastEventHour = () => SkyTime.isEventHour(this.hour) ? this.hour : this.hour - 1;

    /** Checks if current hour is Aurora Concert hour */
    public isConcertHour = () => this.hour == 0 || this.hour % 4 == 0;
}

interface TimeSpan {
    days: number,
    hours: number,
    minutes: number,
    seconds: number
}

/**
 * Converts amount of hours into equivalent amount of hours
 */
export function hoursToSeconds (hours: number) { return hours * 3600 }

/**
 * Converts amount of hours into equivalent amount of hours
 */
export function minutesToSeconds (minutes: number) { return minutes * 60 }

/**
 * Converts combined amount of (hours, minutes, seconds) into seconds
 */
export function hmsToSeconds (hours: number, minutes: number, seconds: number) {
    return (hours * 3600) + (minutes * 60) + seconds;
}

/**
 * Converts combined amount of (days, hours, minutes, seconds) into seconds
 */
export function dhmsToSeconds (days: number, hours: number, minutes: number, seconds: number) {
    return (days * 86400) + hmsToSeconds(hours, minutes, seconds);
}

/**
 * Converts TimeSpan into a string formatted as "dd:hh:mm:ss", skipping the first segment if days are less than 1.
 */
export function humanizeTimeSpan(timespan: TimeSpan) {
    return (timespan.days < 1 ? "" : timespan.days + ":")
        + (timespan.days >= 1 && timespan.hours < 10 ? "0" : "")+ timespan.hours + ":" 
        + (timespan.minutes < 10 ? "0" : "")+ timespan.minutes+ ":" 
        + (timespan.seconds < 10 ? "0" : "") + timespan.seconds;
}

/**
 * Converts amount of seconds into TimeSpan
 * @param secs Amount of seconds
 */
export function secondsToTime(secs: number): TimeSpan {
    secs = Math.round(secs);

    const minutesInSec = 60;
    const hoursInSec = minutesInSec * 60;
    const daysInSec = hoursInSec * 24;

    const days = Math.floor(secs / daysInSec);

    const divisor_for_hours = secs % daysInSec;
    const hours = Math.floor(divisor_for_hours / hoursInSec);

    const divisor_for_minutes = divisor_for_hours % hoursInSec;
    const minutes = Math.floor(divisor_for_minutes / 60);

    const divisor_for_seconds = divisor_for_minutes % 60;
    const seconds = Math.ceil(divisor_for_seconds);

    return { days, hours, minutes, seconds };
}

export function secondsToClock(secs: number): string {
    let result = "";
    for (const char of humanizeTimeSpan(secondsToTime(secs))) result += `<span class="clock">${char}</span>`;
    return result;
}

export function diffNow (object, unit) {
    object.diffNow(unit).toObject()[unit];
}

export function humanizeSeconds(seconds: number) {
    return seconds > 0 ? humanizeTimeSpan(secondsToTime(seconds)) : false;
}

/** Returns DateTime of the next standard Sky event, given exact minute mark for it, assuming events always take place on even hours */
export function getNextEventTime(time: DateTime, minute: number) {

    // If current minute is past the event time, move to the next even hour
    let nextEventHour = time.hour + (time.minute >= minute ? 2 - (time.hour % 2) : time.hour % 2);

    // If next hour is odd, move to the next even hour
    nextEventHour += nextEventHour % 2 !== 0 ? 1 : 0;

    return time.set({ hour: nextEventHour, minute: minute, second: 0, millisecond: 0 });
}

/** Returns DateTime of the next Aurora's Concert */
export function getNextConcertTime(time: DateTime) {
    const nextDate = time.set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).plus({ days: 1 });
    const nextEventHour = Math.floor(time.diff(nextDate, 'hours').hours / -4) * -4;
    return nextDate.plus({ hours: nextEventHour });
}