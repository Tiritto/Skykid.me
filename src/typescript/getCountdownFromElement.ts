export function getCountdownFromElement(element: HTMLElement) {
    return element.getElementsByClassName('countdown')[0] as HTMLOutputElement;
}
