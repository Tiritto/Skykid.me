import { DateTime } from "luxon";
import { SkyTime, secondsToClock } from "./timeFunctions";
import { getCountdownFromElement } from "./getCountdownFromElement";

export class Countdown {
    private _element: HTMLElement;
    private _fnUntil: (arg?: DateTime) => DateTime;
    private _timer: SkyTime;
    private _countdown: Element;

    constructor(skyTimer: SkyTime, identifier: string, fnUntil: (arg?: DateTime) => DateTime) {
        this._element = document.getElementById(identifier);
        this._countdown = this._element.getElementsByClassName("countdown")[0];
        this._fnUntil = fnUntil;
        this._timer = skyTimer;
        this._timer.addEvent(this.update);
    }

    /** Returns amount of seconds until next Event */
    private getTimeRemaining = () => this._fnUntil(this._timer.now).diffNow('seconds').seconds;

    public update =() => getCountdownFromElement(this._element).innerHTML = secondsToClock(this.getTimeRemaining());
}